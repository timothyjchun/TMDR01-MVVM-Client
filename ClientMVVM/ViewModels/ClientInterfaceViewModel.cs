﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using AwesomeSockets.Sockets;
using AwesomeSockets.Domain.Sockets;
using System.Diagnostics;

namespace ClientMVVM.ViewModels
{
    class ClientInterfaceViewModel : INotifyPropertyChanged
    {

        // Models
        private Models.NetworkModel _networkModel;
        private Models.DataModel _dataModel;

        // Commands
        public Commands.StartConnectionCommand StartConnectionCommand { get; set; }
        public Commands.SendDataCommand SendDataCommand { get; set; }



        // Threads
        private Threads.ClientThread ClientThread;
        public CancellationTokenSource cts = new CancellationTokenSource();

        public ClientInterfaceViewModel()
        {
            StartConnectionCommand = new Commands.StartConnectionCommand(StartConnectionExecuteFunction, StartConnectionCanExecuteFunction);
            SendDataCommand = new Commands.SendDataCommand(SendDataExecuteFunction, SendDataCanExecuteFunction);
            _networkModel = new Models.NetworkModel();
            _dataModel = new Models.DataModel();
            _dataModel.UpdateEncodeString();

            ClientThread = new Threads.ClientThread(false,"",0,null,DataModel,NetworkModel);
        }


        public Models.NetworkModel NetworkModel
        {
            get { return _networkModel; }
            set { _networkModel = value; OnPropertyChanged("NetworkModel"); }
        }

        public Models.DataModel DataModel
        {
            get { return _dataModel; }
            set { _dataModel = value; OnPropertyChanged("DataModel"); }
        }



        private void StartConnectionExecuteFunction(object obj)
        {
            if (NetworkModel.IsConnected == false)
            {
                string ipAddress = NetworkModel.IpAddress;
                int port = NetworkModel.Port;

                ClientThread.ipAddress = ipAddress;
                ClientThread.port = port;
                ClientThread.cancellationTokenSource = cts;
                ClientThread.status = true;
                NetworkModel.IsConnected = true;
                Debug.WriteLine("Starting Connection");
                ClientThread.startThread();
            }
            else if(NetworkModel.IsConnected == true)
            {
                bool res = ClientThread.stopThread();
                if (res == true)
                {
                    NetworkModel.IsConnected = false;
                    Thread.Sleep(500);
                    Debug.WriteLine("Closed Client Successfully");
                    cts = new CancellationTokenSource();
                }
                else
                {
                    Debug.WriteLine("Couldn't stop server correctly. Please Try again");
                }
            }
        }

        private bool StartConnectionCanExecuteFunction(object obj) {return true;}
        private void SendDataExecuteFunction(object obj) {
            if (ClientThread.hasConnected == true)
            {
                DataModel.SendData = !DataModel.SendData;
                Debug.WriteLine(DataModel.SendData);
                if(DataModel.SendData == true)
                {
                    ClientThread.sendData = true;
                }
                else
                {
                    ClientThread.sendData = false;
                }
            }
        }

        private bool SendDataCanExecuteFunction(object obj) {
            return true;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
