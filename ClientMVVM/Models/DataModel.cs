﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace ClientMVVM.Models
{
    class DataModel : INotifyPropertyChanged
    {

        private int _data1 = 50;
        public int Data1
        {
            get { return _data1; }
            set { 
                _data1 = value; 
                OnPropertyChanged("Data1");
                UpdateEncodeString();
            }
        }


        private float _data2 = 20.5f;
        public float Data2
        {
            get { return _data2; }
            set { 
                _data2 = value; 
                OnPropertyChanged("Data2");
                UpdateEncodeString();
            }
        }


        private int _data3 = 90;
        public int Data3
        {
            get { return _data3; }
            set { _data3 = value; OnPropertyChanged("Data3"); UpdateEncodeString(); }
        }


        private string _encode = "";
        public string Encode
        {
            get { return _encode; }
            set { _encode = value; OnPropertyChanged("Encode"); }
        }


        private byte[] _parseArray = { };
        public byte[] ParseArray
        {
            get { return _parseArray; }
            set { _parseArray = value; OnPropertyChanged("ParseArray"); }
        }
    

        public void UpdateEncodeString()
        {
            List<byte> parseList = new List<byte>();
            byte[] bdata1 = BitConverter.GetBytes(Data1);
            byte[] bdata2 = BitConverter.GetBytes(Data2);
            byte[] bdata3 = BitConverter.GetBytes(Data3);
            parseList.AddRange(bdata1);
            parseList.AddRange(bdata2);
            parseList.AddRange(bdata3);
            ParseArray = parseList.ToArray();
            Encode = BitConverter.ToString(ParseArray);
        }


        private string _nextSendTime = "00";
        public string NextSendTime
        {
            get { return _nextSendTime; }
            set { _nextSendTime = value; OnPropertyChanged("NextSendTime"); }
        }


        private bool _sendData = false;
        public bool SendData
        {
            get { return _sendData; }
            set { 
                _sendData = value;
                if (_sendData == true)
                {
                    StartStopButtonText = "Stop Sending";
                }
                else StartStopButtonText = "Initiate Start";
                OnPropertyChanged("SendData"); 
            }
            
        }



        private string _startStopButtonText = "Initiate Start";
        public string StartStopButtonText
        {
            get { return _startStopButtonText; }
            set { _startStopButtonText = value; OnPropertyChanged("StartStopButtonText"); }
        }





        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
