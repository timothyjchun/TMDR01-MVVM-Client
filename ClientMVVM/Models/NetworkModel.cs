﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace ClientMVVM.Models {
    class NetworkModel : INotifyPropertyChanged
    {

        private string _ipAddress = "127.0.0.1";
        public string IpAddress
        {
            get { return _ipAddress; }
            set { _ipAddress = value; OnPropertyChanged("IpAddress"); }
        }


        private int _port = 8000;
        public int Port
        {
            get { return _port; }
            set { _port = value; OnPropertyChanged("Port"); }
        }



        private bool _isConnected = false;
        public bool IsConnected
        {
            get { return _isConnected; }
            set { 
                _isConnected = value;
                if(_isConnected == true)
                {
                    IsConnectedColor = "#7DFF80";
                    ConnectButtonContent = "Stop Connection";
                }
                else
                {
                    IsConnectedColor = "#FE7F7D";
                    ConnectButtonContent = "Start Connection";
                } 
            }
        }


        private string _isConnectedColor = "#FE7F7D";
        public string IsConnectedColor
        {
            get { return _isConnectedColor;  }
            set { _isConnectedColor = value; OnPropertyChanged("IsConnectedColor"); }
        }


        private string _connectButtonContent = "Start Connection";
        public string ConnectButtonContent
        {
            get { return _connectButtonContent; }
            set { _connectButtonContent = value; OnPropertyChanged("ConnectButtonContent"); }
        }


        private int _timeConnected = 0;
        public int TimeConnected
        {
            get { return _timeConnected; }
            set { _timeConnected = value; OnPropertyChanged("TimeConnected"); }
        }





        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if(handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
