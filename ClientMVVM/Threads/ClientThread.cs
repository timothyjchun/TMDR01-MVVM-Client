﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AwesomeSockets.Sockets;
using AwesomeSockets.Domain.Sockets;
using System.Diagnostics;
using System.Net.Sockets;
using Buffer = AwesomeSockets.Buffers.Buffer;
using System.Net;
using System.Windows.Controls;
using System.Timers;
using ClientMVVM.Models;

namespace ClientMVVM.Threads
{
    class ClientThread
    {

        public string ipAddress;
        public int port;
        public CancellationTokenSource cancellationTokenSource;
        public bool status; // 연결하라는 명령
        public bool sendData;
        public bool hasConnected; // 연결이 됐는지 안됐는지


        private System.Timers.Timer _timer;


        private Models.DataModel dataModel;
        private Models.NetworkModel networkModel;


        Thread clientThread = null;
        ISocket? server = null;

        public ClientThread(bool Status,string IpAddress, int Port, CancellationTokenSource? cts, Models.DataModel DataModel, Models.NetworkModel NetworkModel)
        {
            status = Status;
            ipAddress = IpAddress;
            port = Port;
            cancellationTokenSource = cts;

            dataModel = DataModel;
            networkModel = NetworkModel;

            sendData = false;

            hasConnected = false;

        }


        public void startThread()
        {
            Debug.WriteLine("starting thread");
            if (status == true)
            {
                clientThread = new Thread(() => runClient(cancellationTokenSource.Token));
                clientThread.IsBackground = true;
                clientThread.Start();
            }
        }

        public bool stopThread()
        {
            if (status == true)
            {
                try
                {
                    Debug.WriteLine("status true => shutting down");
                    status = false;
                    if (server != null && checkSocketAlive(server.GetSocket()))
                    {
                        server.GetSocket().Shutdown(SocketShutdown.Both);
                        server.Close();
                    }
                    cancellationTokenSource.Cancel();
                    clientThread.Join();
                    cancellationTokenSource.Dispose();

                    status = false;
                    sendData = false;
                    hasConnected = false;

                    return true;
                }
                catch(Exception e)
                {
                    Debug.WriteLine(e);
                    return false;
                }
            }
            return false;
        }



        private void runClient(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                while (hasConnected == false && !token.IsCancellationRequested)
                {
                    try
                    {
                        server = AweSock.TcpConnect(ipAddress, port);
                        Debug.WriteLine("Has Connected");
                        hasConnected = true;
                        //StartTimer(1000);
                        while (hasConnected == true && !token.IsCancellationRequested)
                        {
                            while (sendData == true && !token.IsCancellationRequested)
                            {
                                Buffer outBuff = Buffer.New(dataModel.ParseArray.Length);
                                Debug.WriteLine("Sending Data....");
                                dataModel.NextSendTime = "03";
                                Thread.Sleep(1000);
                                dataModel.NextSendTime = "02";
                                Thread.Sleep(1000);
                                dataModel.NextSendTime = "01";
                                Thread.Sleep(1000);

                                //Buffer에 데이터 싣기
                                Buffer.ClearBuffer(outBuff);
                                Buffer.Add(outBuff, dataModel.ParseArray); // 데이터 Buffer에 추가
                                Buffer.FinalizeBuffer(outBuff);
                                AweSock.SendMessage(server,outBuff);
                            }
                            Thread.Sleep(100);
                        }
                        dataModel.NextSendTime = "00";
                        //StopTimer();
                    }
                    catch (Exception e)
                    {
                        hasConnected = false;
                        Debug.WriteLine("Failed to connect. Trying again");
                    }
                    Thread.Sleep(100);
                }
                Thread.Sleep(100);
            }
            if(status == true)
            {
                stopThread();
            }
        }


        private bool checkSocketAlive(Socket s)
        {
            try
            {
                bool part1 = s.Poll(1000, SelectMode.SelectRead);
                bool part2 = (s.Available == 0);
                if (part1 && part2)
                    return false;
                else
                    return true;
            }
            catch(Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
        }




        private void StartTimer(int interval)
        {
            _timer = new System.Timers.Timer(interval);
            _timer.Elapsed += OnTimerElapsed;
            _timer.AutoReset = true; // Set to true for repeated firing
            _timer.Start();
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            Debug.WriteLine("연결된 시간 : " + networkModel.TimeConnected);
            networkModel.TimeConnected += 1;
        }

        public void StopTimer()
        {
            networkModel.TimeConnected = 0;
            _timer.Stop();
            _timer.Dispose();
            Debug.WriteLine("Timer Shutting Down..");
        }


    }

}
